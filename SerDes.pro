#-------------------------------------------------
#
# Project created by QtCreator 2016-04-29T11:59:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = SerDes
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    main.h
