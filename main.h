#ifndef MAIN
#define MAIN
#include <QDataStream>
#include <QMetaProperty>

//namespace SERDES {

///
/// \brief The Serialyze / Deserialize class by Qt Meta Objects 1
///
class SerDes: public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Version", "1.0.0")
public:    
    SerDes(QObject *parent = 0);
};

QDataStream& operator<<(QDataStream& ds, const SerDes& obj);

QDataStream& operator>>(QDataStream& ds, SerDes& obj);

///
/// \brief The Victim class for SerDes
///
class Victim: public SerDes
{
    Q_OBJECT
    Q_PROPERTY(int id READ getId WRITE setId)
    Q_PROPERTY(QString Name READ getName WRITE setName)
public:
    ///
    /// \brief Victim constructor
    ///
    Victim();
    ///
    /// \brief getting Id value
    /// \return Id value
    ///
    int getId() const { return _id; } /// <get
    void setId(int newId) { _id = newId; }
    QString getName() const { return _name; }
    void setName(const QString &newName) { _name = newName; }

private:
    int _id;
    QString _name;
};

//Q_DECLARE_METATYPE(Victim)

//}
#endif // MAIN

