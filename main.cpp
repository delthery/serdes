#include <QCoreApplication>
#include <QDebug>

#include "main.h"

//using namespace SERDES;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Victim v1, v2;
    v1.setId(1);
    v1.setName("First victim");

    QByteArray ba;
    QDataStream out(&ba, QIODevice::WriteOnly);

    qDebug() << "v1:" << v1.getId() << v1.getName();

    out << v1;
    qDebug() << "Byte Array Size" << ba.size();

    QDataStream in(ba);
    in >> v2;

    qDebug() << "v2:" << v2.getId() << v2.getName();

    //return a.exec();
}


SerDes::SerDes(QObject *parent) : QObject(parent)
{
    setObjectName("SerDes");
}


Victim::Victim() : SerDes(){}

QDataStream &operator<<(QDataStream &ds, const SerDes &obj)
{
    qDebug() << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
    QVariant var;
    int stored = 0;

    for(int i=0; i < obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            var = obj.metaObject()->property(i).read(&obj);
            ds << var;
            qDebug() << obj.metaObject()->property(i).name() << var << var.toByteArray().toHex();
            stored++;
        }
    }
    qDebug() << "Serialize" << stored << "from" << obj.metaObject()->propertyCount() << "properties";
    return ds;
}

QDataStream &operator>>(QDataStream &ds, SerDes &obj)
{
    qDebug() << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
    int stored = 0;
    QVariant var;
    for(int i=0; i < obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds >> var;

            bool status = obj.metaObject()->property(i).write(&obj, var);
            qDebug() << obj.metaObject()->property(i).name() << var << var.toByteArray().toHex();
            qDebug() << status;
            stored++;
        }
    }
    qDebug() << "Deserialize" << stored << "from" << obj.metaObject()->propertyCount() << "properties";
    return ds;
}
